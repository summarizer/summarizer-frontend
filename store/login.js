export const state = () => ({
  key: '',
  url: ''
})

export const getters = {
  isReady: state => {
    if (state.key === '') return false
    return state.url !== ''
  },
  isNotReady: (state, getters) => !getters.isReady
}

export const actions = {
  load({ commit }) {
    const key = localStorage.getItem('KEY') || ''
    const url = localStorage.getItem('URL') || ''
    commit('set', { key, url })

  },
  persist({ commit }, { key, url }) {
    localStorage.setItem('KEY', key)
    localStorage.setItem('URL', url)
    commit('set', { key, url })
  }
}

export const mutations = {
  set(state, { key, url }) {
    state.key = key
    state.url = url
  }
}

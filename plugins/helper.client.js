export default ({ app }, inject) => {
  inject('helper', new Helper(app))
}


class Helper {
  constructor(app) {
    this.app = app
  }

  shortenTitle(title = '') {
    if (title.length < 42) return title

    return title.substring(0, 39) + '...'
  }
}

export default {
  welcome: 'Welcome',
  next: 'Next',
  back: 'Back',
  first: 'First',
  last: 'Last',
  search: 'Search',
  hitsPerPage: '{0} hits per page',
  homepage: 'Homepage',
  key: 'API Key',
  url: 'URL',
  credentials: 'Credentials',
  submit: 'Submit',
  showMore: 'Show more',
  scrollToBottom: 'Scroll down',
  scrollToTop: 'Scroll up'
}

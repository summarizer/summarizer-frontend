FROM ranadeeppolavarapu/nginx-http3:latest

COPY nuxt-build/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/
COPY default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
